<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page buffer="8kb" %>
<%@ page info="Chat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<%
   //Avaya Aura Contact Center Multimedia 
   //Copyright � 2014 Avaya Inc. All Rights Reserved
   
   // This page contains an embedded form to manage the reaction
   // to browser close events i.e. abandon queuing the chat request 
   // to an agent if the customer is still holding or send a disconnect 
   // message to the agent if active in a session.


   String 		sessionKey;
   String 		contactID;

   request.setCharacterEncoding("utf-8");
	sessionKey=session.getAttribute("sessionKey").toString();
	contactID=session.getAttribute("contactID").toString();

%>

<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title></title>

  <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
  <link rel="stylesheet" href="<c:url value="/css/chatdisplay.css" />">
  
    <script type="text/javascript">

	function handleOnClose()
	{
	    exitform.inSession.value = parent.statuscontrol.document.statusform.inSession.value;    
	}

    </script>

</head>

<body onbeforeunload="handleOnClose()">
    
    <form name="chatform" method="post">
    
        <div id="colunas" class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
	  			<table>
	          		<tr>
	          			<th>
	          				<ul id="messages">
	          					<div id="chatterbox" name="history" class="webchat_history"></div>
	          				</ul>
	          			</th>
	          		</tr>
	          	</table>
    	</div>
                
    </form>
    
<form name="exitform" method="post" action="exitFormImpl64.jsp">
	<input type="hidden" name="sessionKey" value="<%=sessionKey%>">
	<input type="hidden" name="contactID" value="<%=contactID%>">
	<input type="hidden" name="inSession" value="0">
</form>

</body>

</html>

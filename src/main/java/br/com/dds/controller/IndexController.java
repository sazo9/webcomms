package br.com.dds.controller;


import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nortel.applications.ccmm.ci.datatypes.CIContactPriority;
import com.nortel.applications.ccmm.ci.datatypes.CIContactWriteType;
import com.nortel.applications.ccmm.ci.datatypes.CICustomerReadType;
import com.nortel.applications.ccmm.ci.datatypes.CISkillsetReadType;
import com.nortel.applications.ccmm.ci.webservices.AnonymousLoginResult;
import com.nortel.applications.ccmm.ci.webservices.CICustomerWs;
import com.nortel.applications.ccmm.ci.webservices.CICustomerWsLocator;
import com.nortel.applications.ccmm.ci.webservices.CICustomerWsSoap;
import com.nortel.applications.ccmm.ci.webservices.CISkillsetWs;
import com.nortel.applications.ccmm.ci.webservices.CISkillsetWsLocator;
import com.nortel.applications.ccmm.ci.webservices.CISkillsetWsSoap;
import com.nortel.applications.ccmm.ci.webservices.CIUtilityWs;
import com.nortel.applications.ccmm.ci.webservices.CIUtilityWsLocator;
import com.nortel.applications.ccmm.ci.webservices.CIUtilityWsSoap;
import com.nortel.applications.ccmm.ci.webservices.CIWebCommsWs;
import com.nortel.applications.ccmm.ci.webservices.CIWebCommsWsLocator;
import com.nortel.applications.ccmm.ci.webservices.CIWebCommsWsSoap;

import br.com.dds.config.Config;
import br.com.dds.dao.DAO;
import br.com.dds.model.Cliente;
import br.com.dds.model.Pesquisa;

@Controller
@RequestMapping("WebComms")
public class IndexController{

	@Autowired//( required = false )
    private DAO<Pesquisa> pesquisaDAO;

    private final 	Logger log;
    private String 	sessionKey;
    private long 	contactID = -2;
    private long 	commSessioin;
    private String 	ccmmHostname;
	private String 	webCommsHost;
	private String 	webCommsPort;
	private String 	webCommsPath;
	private String 	defaultSkillsetName;

    public IndexController() throws IOException {
        this.log = LoggerFactory.getLogger(this.getClass());
		Config config = new Config();
		Properties properties = config.getProp();

		this.ccmmHostname = properties.getProperty("CCMM_HOSTNAME");
		this.webCommsHost = properties.getProperty("WEBCOMMS_HOSTNAME");
		this.webCommsPort = properties.getProperty("WEBCOMMS_PORT");
		this.webCommsPath = properties.getProperty("WEBCOMMS_PATH_ANONYMOUS");
		this.defaultSkillsetName = properties.getProperty("WEBCOMMS_SKILLSET");
    }


    @RequestMapping("/teste")
    String indexTESTE(){
        return "TESTE";
    }

	@RequestMapping("/chat/pesquisa")
	public String indexPesquisa() {
		return "Pesquisa";
	}

	@RequestMapping(value="/save", method=RequestMethod.POST)
	@ResponseBody
	public String saveClient(Pesquisa pesquisa, HttpSession session) {

		if (session.getAttribute("email") != null){
			pesquisa.setAni(session.getAttribute("email").toString());
		}else{
			log.error(">>> EMAIL NAO CONSTA NA SESSAO");
			pesquisa.setAni("");
		}

		if (session.getAttribute("cpf") != null){
			pesquisa.setContaContrato(session.getAttribute("cpf").toString());
		}else{
			log.error(">>> CPF NAO CONSTA NA SESSAO");
			pesquisa.setContaContrato("");
		}

		pesquisa.setDataHora(new Date());
		pesquisa.setNomeApp("Chat");
		log.info("========================== INSERINDO DADOS PESQUISA ========================");
		log.info("Agente: " + pesquisa.getAgente());
		log.info("Nota1: " + pesquisa.getPergunta1());
		log.info("Nota2: " + pesquisa.getPergunta2());
		log.info("Nota3: " + pesquisa.getPergunta3());
		log.info("Nota4: " + pesquisa.getPergunta4());
		log.info("Email: " + pesquisa.getAni());
		log.info("CPF: " + pesquisa.getContaContrato());
		log.info("DataHora: " + pesquisa.getDataHora());

		pesquisaDAO.save(pesquisa);
		return "Pesquisa salva com sucesso!";
	}

	@RequestMapping(value={"/chat/exitFormImpl64"})
	public String exitFormImpl64(HttpSession session, HttpServletRequest request) throws IOException {
		if (request.getAttribute("isSessionActive") != null){
			return "exitFormImpl64.jsp?sessionKey=" + this.sessionKey + "&contactID=" + this.contactID + "&inSession=" + request.getAttribute("isSessionActive");
		}else{
			return "redirect:Pesquisa";
		}
	}

	@RequestMapping(value={"/chat/chatdisplay64"})
	public String chatdisplay64(HttpSession session) throws IOException {
		return "chatdisplay64.jsp?sessionKey=" + this.sessionKey + "&contactID=" + this.contactID;

	}

	@RequestMapping(value={"/chat/status64"})
	public String status64(HttpSession session) throws IOException {
		return "status64.jsp?sessionKey=" + this.sessionKey + "&contactID=" + this.contactID;

	}

	@RequestMapping(value={"/chat/hidden64"})
	public String hidden64(HttpSession session, HttpServletRequest request) throws IOException {
		return "hidden64.jsp?sessionKey=" + this.sessionKey + "&contactID=" + this.contactID;

	}

	@RequestMapping(value={"/chat/heartbeat"})
	public String heartbeat(HttpSession session) throws IOException {
		return "heartbeat.jsp?sessionKey=" + this.sessionKey + "&contactID=" + this.contactID;

	}

	@RequestMapping(value={"/chat/chatwrite64"})
	public String chatwrite64(HttpSession session) throws IOException {
		return "chatwrite64.jsp?sessionKey=" + this.sessionKey + "&contactID=" + this.contactID;

	}


	@RequestMapping(value={"/chat/onhold64"}, method=RequestMethod.GET)
	public String onhold64() throws IOException {
		return "onhold64";

	}

	@RequestMapping(value={"/chat/inchat64"}, method=RequestMethod.GET)
	public String inchat64() throws IOException {
		return "inchat64";

	}

	@RequestMapping(value={"/chat/banner"})
	public String banner() throws IOException {
		return "banner";

	}

	@RequestMapping(value={"/chat"})
	public String indexLogint() {

//		  ModelAndView model = new ModelAndView();
//		if ( cpf.equals("") ){
//			log.info(">>> CLIENTE NAO IDENTIFICADO: " + cpf);
//			model.setViewName("WebChatLogin");
//		}else{
//			log.info(">>> CLIENTE IDENTIFICADO: " + cpf);
//			model.setViewName("Assunto");
//		}

		return "WebChatLogin";
	}

	@RequestMapping(value={"/chat/assunto"})
	public String indexAssunto(Cliente cliente, HttpServletRequest request, HttpSession session) throws IOException {
System.out.println(cliente.getUsername());
		try{
			session.setAttribute("username", cliente.getUsername());
			session.setAttribute("cpf", cliente.getCpf());
			session.setAttribute("email", cliente.getEmail());
		}catch (Exception e) {
			log.error(e.getMessage());
			return "erro";
		}
		
		return "Assunto";
	}


	@RequestMapping("/displayHeaderInfo.do")
	public @ResponseBody String displayHeaderInfo(@CookieValue("JSESSIONID") String cookie)  {
	  return cookie;
	}

	@RequestMapping(value ={"/chat/start"}, method = RequestMethod.POST)
	public String startChat(Cliente cliente, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ServiceException {
		
		String ret = "erro";
		
		try{
			
			session.setAttribute("CCMM_HOSTNAME", ccmmHostname);
			session.setAttribute("WEBCOMMS_HOSTNAME", webCommsHost);
			session.setAttribute("WEBCOMMS_PORT", webCommsPort);
			session.setAttribute("WEBCOMMS_PATH_ANONYMOUS", webCommsPath);
			session.setAttribute("WEBCOMMS_SKILLSET", defaultSkillsetName);

			ret = redirectWebChat(request, response, session);
			
		}catch (Exception e) {
			log.error(e.getMessage());
			return "erro";
		}


		return ret;
	}

	public String redirectWebChat(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {

		String retorno = "";

		try{
			
			String username = session.getAttribute("username").toString();
			String email = session.getAttribute("email").toString();
			String cpf = session.getAttribute("cpf").toString();
			String skillset = request.getParameter("skillset");

			log.info(">>> HOST: " + webCommsHost);
			log.info(">>> PORT: " + webCommsPort);
			log.info(">>> PATH: " + webCommsPath);
			log.info(">>> DEFAULT SKILL: " + defaultSkillsetName);

			log.info(">>> userName: " + username);
			log.info(">>> CPF: " + cpf);
			log.info(">>> email: " + email);
			log.info(">>> skillset: " + skillset);

			long myCustomerID = 0;
			
			// CIUtility Web Services:
			java.net.URL ciUtilityWSUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CIUtilityWs.asmx");
			CIUtilityWs utilityWsLocator = new CIUtilityWsLocator();
			CIUtilityWsSoap utilityWs = utilityWsLocator.getCIUtilityWsSoap(ciUtilityWSUrl);
//			session.setAttribute("utilityWs", utilityWs);

			// customer web service URL
			java.net.URL ciCustomerWsUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CICustomerWs.asmx");
			CICustomerWs customerWsLocator = new CICustomerWsLocator();
			CICustomerWsSoap customerWs = customerWsLocator.getCICustomerWsSoap(ciCustomerWsUrl);
//			session.setAttribute("customerWs", customerWs);

			// skillset web service URL
			java.net.URL ciSkillsetWsUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CISkillsetWs.asmx");
			CISkillsetWs skillsetWsLocator = new CISkillsetWsLocator();
			CISkillsetWsSoap skillsetWs = skillsetWsLocator.getCISkillsetWsSoap(ciSkillsetWsUrl);
//			session.setAttribute("skillsetWs", skillsetWs);


			// 1. Get an anonymous sesssion key:
			// ================================
			AnonymousLoginResult result = utilityWs.getAnonymousSessionKey();
			sessionKey = result.getSessionKey();
            log.info(">>> sessionKey: " + sessionKey);

			// 2. Populate Customer Details:
			// ==============================
			CICustomerReadType ciCustomer = new CICustomerReadType();

			//ciCustomer.setUsername(name);
			ciCustomer.setFirstName(username); // Set the FirstName as the user name.
			ciCustomer.setLastName("");
			// Note: Address list is not required.

			// 2. Build up a customer from the customer entered details:
			myCustomerID = utilityWs.getAndUpdateAnonymousCustomerID(result, email, "", ciCustomer);

			// 3. Get the skillset ID:
			CISkillsetReadType myCISkillsetReadType = new CISkillsetReadType();
			//myCISkillsetReadType = skillsetWs.getSkillsetByName(skillsetName, result.getSessionKey()); //WC_Default_Skillset
            myCISkillsetReadType = skillsetWs.getSkillsetByName(defaultSkillsetName, result.getSessionKey()); //WC_Default_Skillset



			long mySkillsetID = myCISkillsetReadType.getId();

			// 4. Create the contact:
			// --------------------------------------------
			CIContactWriteType writeContact = new CIContactWriteType();
			writeContact.setSkillsetID(mySkillsetID);
			writeContact.setPriority(CIContactPriority.Priority_3_Medium_High); // Can be read from config file.
			// writeContact.setTimezone(-999);
			writeContact.setSubject(skillset); // ASSUNTO
			writeContact.setTextHTML(""); // Text can be blank


			// check if the Text Chat Skillset is in Service
			if (skillsetWs.isSkillsetInService(mySkillsetID, sessionKey)) {
				// create a new Text Chat Contact
				contactID = customerWs.requestTextChat(myCustomerID, writeContact, false, sessionKey);
                log.info("contactID: " + contactID);

				// Check for max limits:
				// This section cheks for connection maximum limits. Also re-confirms that teh skillset is in service
				if (contactID != -1) {
                    log.info("CONTACT ID != -1");

					//	Check if a text chat session has failed due to max concurrent sessions
					if (contactID == -2) {
                         log.info("CONTACT ID == -2");
						//	Establishing a text chat session has failed: display message and close
                         parseError("webchat_max_concurrent_per_customer");
						 retorno = "erro"; //"";

					} else if (contactID == -3) {
                        log.info("CONTACT ID == -3");
						//	Establishing a text chat session has failed: display message and close
                        parseError("webchat_max_concurrent_per_skillset");
                        retorno = "erro"; // "";
					} else {
                        log.info("CONTACT ID ");
						// Redirect to the Web Chat Application:
						//Build the Web Comms URL from the above values
						//Append the SessionKey and ContactID as parameters to the Web Comms URL

						java.net.URL ciWebCommsWsUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CIWebCommsWs.asmx");
						log.info(">>> ciWebCommsWsUrl: " + ciWebCommsWsUrl);

						CIWebCommsWs webCommsWsLocator = new CIWebCommsWsLocator();
						CIWebCommsWsSoap webCommsWs = webCommsWsLocator.getCIWebCommsWsSoap( ciWebCommsWsUrl );
						log.info(">>> webCommsWs " + webCommsWs);

						commSessioin = webCommsWs.createWebCommsSession(new Long(contactID).longValue(), sessionKey);
						log.info(">>> result " + commSessioin);

						String webCommsURL = "http://" + webCommsHost + ":" + webCommsPort + "/" + webCommsPath;

						//Append the SessionKey and ContactID as parameters to the Web Comms URL
						webCommsURL = "index64.jsp?sessionKey=" + this.sessionKey + "&contactID=" + this.contactID;

						session.setAttribute("sessionKey", sessionKey);
						session.setAttribute("contactID", contactID);
                         retorno = webCommsURL;
					}
				} else {
					//	Skillset not in use: display message and close
					parseError("webchat_no_agents");
					retorno = "erro"; // "";
				}

			} else { //Skillset is not in service, display a message to the user

				// create a closed Text Chat contact for reporting purposes
				contactID = customerWs.requestTextChat(myCustomerID, writeContact, true, result.getSessionKey());
				parseError("webchat_skillset_oos");
				retorno = "erro"; // "";
			}
		}catch (Exception e) {
			parseError("webchat_universal_error");
			log.error(e.getMessage());
            retorno = "erro";
            
		}
		return retorno;
	}


	private String parseError(String errCode){
		String errMessage = "";

	    if(errCode.equals("webchat_max_concurrent_per_customer"))
	    {
	        // Display the following message:
	        errMessage = "Unable to connect chat session: You have reached a maximum of concurrent chat sessions.";
	    }
	    else if (errCode.equals("webchat_max_concurrent_per_skillset"))
	    {
	        errMessage = "Unable to connect chat session: There are currently no agents available to answer your chat.";
	    }
	    else if (errCode.equals("webchat_no_agents"))
	    {
	        errMessage = "Unable to connect chat session: There are no agents available to answer your chat at this time.";
	    }
	    else if (errCode.equals("webchat_skillset_oos"))
	    {
	        errMessage = "Unable to connect chat session: There are no agents available to answer your chat at this time.";
	    }
	    else if (errCode.equals("webchat_universal_error"))
	    {
	    	 errMessage = "Falha ao carregar chat.";
	    }

	    log.error(">>> " + errMessage);
	    
	    return errMessage;
	}


}

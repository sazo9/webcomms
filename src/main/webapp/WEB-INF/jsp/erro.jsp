<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title></title>

  <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
  <link rel="stylesheet" href="<c:url value="/css/assunto.css" />">
</head>

  <script src="<c:url value="/js/jquery-3.1.0.min.js" />"></script>
  <script src="<c:url value="/js/bootstrap.min.js" />"></script>

<body>
  <div class="login-form col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
    <header>
  		<h1><img class="img-responsive" src="<c:url value="/img/fundo.png"/>"></h1>
  	</header>
    <div class="painel-erropanel panel-danger">
      <div class="alert alert-danger text-center" style="padding-bottom: 0px;">
          <span class="glyphicon glyphicon-exclamation-sign"></span>
          <h2>No momento nenhum dos operados estão online </h2>
      </div>
    </div>
  </div>
</body>
</html>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.nortel.applications.ccmm.ci.datatypes.*"%>
<%@page import="com.nortel.applications.ccmm.ci.webservices.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<%
String ccmmHostname  = application.getInitParameter("CCMM_HOSTNAME");

String sessionKey = (String)session.getAttribute("sessionKey");

java.net.URL ciSkillsetWsUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CISkillsetWs.asmx"); 

CISkillsetWs skillsetLocator = new CISkillsetWsLocator();
CISkillsetWsSoap skillsetWs = skillsetLocator.getCISkillsetWsSoap( ciSkillsetWsUrl );

CIMultipleSkillsetsReadType skillsetReadResult = new CIMultipleSkillsetsReadType();
skillsetReadResult = skillsetWs.getAllWebSkillsets(sessionKey);

int length = skillsetReadResult.getSkillsetList().length;

%>
    <select name="Skillset">
    <option selected value="none">(Please Select)</option>
<%
    for(int i=0; i<length; i++)
    {
        String skillsetName = skillsetReadResult.getSkillsetList()[i].getName();
        
        long skillsetID = skillsetReadResult.getSkillsetList()[i].getId();
        
        %><option value="<%=skillsetID%>"><%=skillsetName%></option>
    <%
    }
    

%>
    </select>&nbsp;<font class="formlabel" color="red">*</font>









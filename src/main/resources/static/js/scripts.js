// ######################  hidden  ######################## 
		String.prototype.trim = function() 
        {
              // skip leading and trailing whitespace
              // and return everything in between
               var x=this;
               x=x.replace(/^\s*(.*)/, "$1");
               x=x.replace(/(.*?)\s*$/, "$1");
               return x;
        }

        function giveWebOnHold()
        {
             // If we are already in Web On Hold, do nothing
             // otherwise redirect our browser to onhold.jsp
             parent.statuscontrol.document.statusform.inSession.value = "0";

             var theurl = ""; 
             theurl = '' + parent.maincontrol.location;

             if(document.all)
             {
                     // Internet Explorer (all versions)

                     if(theurl.indexOf("top.jsp") > 0)
                     {
                        parent.maincontrol.location = 'onhold64';
                     }
             }
             else
             {
                     // Netscape (all versions)
                     if(theurl.indexOf("top.jsp") > 0)
                     {
                        parent.maincontrol.location.href = 'onhold64';
                     }
             }
        }


        function giveChatTreatment()
        {
             // We have retrieved message(s) from our session. Navigate away
             // from Web On Hold to the inchat64.jsp page

             var theurl = ""; 
             theurl = '' + parent.maincontrol.location
             parent.statuscontrol.document.statusform.status.value = "Status: Active";
             parent.statuscontrol.document.statusform.inSession.value = "1";

             if(theurl.indexOf("inchat64.jsp") == -1)
             {
                     parent.maincontrol.location = 'inchat64'

             }

             parent.statuscontrol.document.statusform.expectedWaitTime.value = ""
             parent.statuscontrol.document.statusform.positionInQueue.value =  ""
        }


        function setfocus()
        {
            parent.chatwrite.document.chatform.customerchat.focus();
        }


        function displayPushedPage( str )
        {
            
             if(document.all)
             {
                     // Internet Explorer (all versions)

                     var theurl = ""; 
                     theurl = '' + parent.maincontrol.location;

                     parent.statuscontrol.document.statusform.inSession.value = "1";
                     parent.maincontrol.location = str;

             }
             else
             {
                     // Netscape (all versions)
                     parent.statuscontrol.document.statusform.inSession.value = "1";
                     parent.maincontrol.location.href = str;

             }
        }
	   
        function changeContent(id,shtml) 
        {
           if (parent.chatdisplay.document.getElementById || parent.chatdisplay.document.all) 
           {
              var el = parent.chatdisplay.document.getElementById? parent.chatdisplay.document.getElementById(id): parent.chatdisplay.document.all[id];
              if (el && typeof el.innerHTML != "undefined") el.innerHTML = unescape(shtml);
           }
        }
		

// ######################  chatwrite  ########################
  NS4 = (document.layers) ? true : false;

  function checkEnter(event)
  { 	
	// If the enter key was pressed, send the message or assume a page preview.
	var code = 0;
       
	if (NS4)
		code = event.which;
	else
		code = event.keyCode;
            
	if (code==13)
	{
		sendChat();
		parent.chatwrite.document.chatform.submit();
                //parent.statuscontrol.RefreshChatHistory();
	}
        else
        {
           // NOTE: This exmaple implements the "Customer Is Typing"  feature
           // each time a key is pressed. To reduce the load on the server, this can
           // be reduced by only allowing this once per 'pre-submit' time interval.
            
           // Check if the customer is typing in the chat text-area: 
           // If text remmains in the text-area inform the agent that the
           // customer is typing a message:
           if (document.chatform.customerchat.value.length > 0)
           {
               parent.statuscontrol.InformCustomerIsTyping(true);
           }
           else // Customer has removed any text and is no longer typing: 
           {
               parent.statuscontrol.InformCustomerIsTyping(false);
           }
        }
  }
  
  
  function setfocus()
  {
  	document.chatform.customerchat.focus();
        

  }


  function sendChat()
  {
    // Send the chat message only if there is an active session and there is a message to send.

    if(parent.statuscontrol.document.statusform.inSession.value=="0")
    {
	alert("Não é possivel enviar a mensagem pois não existe um agente ativo nesta conversa.");
    }
    else
    {
    	
	if(parent.chatwrite.document.chatform.customerchat.value=="")
	{
	
	  // do nothing as the user has not entered a chat message
	  parent.chatwrite.document.chatform.message.value = "null";

	}
	else
	{
	  parent.chatwrite.document.chatform.customerchat.value = " " + parent.chatwrite.document.chatform.customerchat.value;
	  parent.chatwrite.document.chatform.message.value = parent.chatwrite.document.chatform.customerchat.value;
	  parent.chatwrite.document.chatform.type.value = "Chat_Message_from_Customer";
	  parent.chatwrite.document.chatform.hiddenMessage.value = "";
	  parent.chatwrite.document.chatform.customerchat.value = "";
          
          
	  message = null;
	}
    }
  }

  function printChat()
  {
     window.parent.chatdisplay.focus();
     window.print();
  }

  function exitChat()
  {
  	// Terminate the session if the user confirms.
  
  	var isSessionActive = parent.statuscontrol.document.statusform.inSession.value;
	var answer = confirm ("This will close your Web Chat session. Do you wish to continue?") 
	if (answer) 
	{
		// Close the session:
		// Session cleanup code is contained in exitFormImpl64.jsp
		
		parent.location="exitFormImpl64.jsp?sessionKey=<%=session.getAttribute(\"sessionKey\").toString()%>&contactID=<%=session.getAttribute(\"contactID\").toString()%>&inSession=" + isSessionActive;	   
	}
  }
  
  function checkSpace(event)
  { 	
	return event.which !== 32;
  }
  

// #################### chatdisplay64 ######################
	function handleOnClose()
	{
	    exitform.inSession.value = parent.statuscontrol.document.statusform.inSession.value;    
	}  
	
// #################### index ######################
    var elementID;

    // Resize the window to account for advert area & webchat history
    function onLoadDo()
    {
            //	On window load resize
            resizeWindow(1100, 1000, 1000, 692, 633, 606, 0);
    }

    //	Resize pop-up according to browser used, and by possible extending length
    function resizeWindow(ffPixelWidth, crPixelWidth, iePixelWidth, ffPixelLength, crPixelLength, iePixelLength, extendLength)
    {
            if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))
                    window.resizeTo(ffPixelWidth, ffPixelLength + extendLength);	//	Firefox
            else if(navigator.userAgent.indexOf("Chrome") != -1)
                    window.resizeTo(crPixelWidth, crPixelLength + extendLength);	//	Chrome
            else if(navigator.appName == "Microsoft Internet Explorer")
            {
                    extendLength -= extendLength/14;
                    window.resizeTo(iePixelWidth, iePixelLength + extendLength);	//	IE
            }
            else 
                    window.resizeTo(iePixelWidth, iePixelLength + extendLength);	//	Resize others as of IE
    }

    // Called by chat frameset to close the chat window when the agent is finished
    function closeChatWindow()
    {
            //	Exit the chat window
            window.close();
    }	
	
// #################### status ######################	

       // This function is called from the customer chat window.
        // It sets a "customerTyping" hidden field and POSTS to the
        // hiden64.jsp file. Based on this value, the AACC server is
        // notified of the customer typing status.
        function InformCustomerIsTyping(bIsTyping)
        {
            if (bIsTyping)
            {
                document.statusform.customerTyping.value = "1";
            }
            else       
            {
                document.statusform.customerTyping.value = "0";
            }
            
            document.statusform.submit();
        }
        
        // This function is used to reload the hidden64.jsp page.
        // Typically called afater a Customer submits a chat text from
        // the chat text area.
        function RefreshChatHistory()
        {
            // Ensure that customerTyping is not set.
            document.statusform.customerTyping.value = "0";
            // submit and POST to hidden64.jsp.
            document.statusform.submit();
        }
        

////
////import org.hibernate.HibernateException;
////import org.hibernate.SessionFactory;
////import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
////import org.hibernate.cfg.Configuration;
////
////public class HibernateUtil {
////	
////	private static SessionFactory sessionFactory;
////	
////	private static SessionFactory configureSessionFactory() throws HibernateException {
////		
////		   Configuration cfg = new Configuration().configure();
////		   StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
////		   sessionFactory = cfg.buildSessionFactory(builder.build());
////		   
////		   return sessionFactory;
////		}
////		 
////	
////	public static SessionFactory getSessionFactory() {
////	   return configureSessionFactory();
////	}
////
////}
//
//
package br.com.dds.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static SessionFactory sessionFactory;
//	private static ServiceRegistry serviceRegistry;

	public static SessionFactory getSessionFactory() throws HibernateException{
		if (sessionFactory == null) {
			Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
			sessionFactory = configuration.buildSessionFactory(builder.build());
		}
		return sessionFactory;
	}
	
//	private static SessionFactory configureSessionFactory() throws HibernateException {
//		if (sessionFactory == null) {
//			Configuration cfg = new Configuration();
//			cfg.configure("hibernate.cfg.xml");
//			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties()).build();
//			sessionFactory = cfg.buildSessionFactory(serviceRegistry);
//		}
//		   return sessionFactory;
//	}
//			 
//			 
//		public static SessionFactory getSessionFactory() {
//		    return configureSessionFactory();
//		}
		
		

}

<%-- 
Document   : Pesquisa
Created on : 08/07/2016, 10:21:08
Author     : sazonov

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title></title>

  <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
  <link rel="stylesheet" href="<c:url value="/css/pesquisa.css" />">
</head>
<body onload="onLoginLoadDo()">

  <div class="login-form col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
    <header>
  		<img class="img-responsive" src="<c:url value="/img/fundo.png"/>">
  		<h2 class="text-center">
  			<legend>Pesquisa de Satisfação</legend>
  		</h2>
    </header>

  	<form  name="requestPesquisa" id="novaPesquisaForm" action="/WebComms/save" method="POST">
    
    <div class="row">
        <div class="col-xs-8">
          <h2>Sua solicitação foi esclarecida?</h2>
        </div>
        <div class="col-xs-2">
          <h2><input type="radio" name="p1" value="Sim" />  Sim</h2>
        </div>
        <div class="col-xs-2">
          <h2><input type="radio" name="p1" value="Nao" />  Nao</h2>
        </div>
      </div>


      <div class="row">

        <div class="col-xs-8">
          <h2>Qual grau de satisfação com o atendimento?</h2>
        </div>
        <div class="col-xs-4">
          <h2>
            <select class=" form-control input-sm"  id="p2" name="p2">
              <option value="0">Nota</option>
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </select>
        </h2>
        </div>
      </div>



      <div class="row">
        <div class="col-xs-8">
          <h2>Qual a probabilidade de você recomendar a Beleza na Web para seus amigos?</h2>
        </div>
        <div class="col-xs-4">
          <h2>
              <select class="form-control input-sm"   id="p3" name="p3">
                <option value="0">Nota</option>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>

          </h2>
        </div>
      </div>


      <input type="hidden" name="agente" value="<%=session.getAttribute("agente") %>" />



  		<footer>
  			<button type="submit" class="btn btn-primary col-xs-6 col-xs-offset-5">Enviar</button>
  		</footer>

<script type="text/javascript"> 


function onLoginLoadDo()
{
//	On window load resize
resizeWindow(438, 438, 438, 415, 433, 397, 0);

}


function processChatRequest()
{

//	Retrieve platform and browser information to pass to agent
var WebCommsQuery = "PLATFORM: [" + navigator.platform + "]; BROWSER: [" + navigator.appName + "]; HEADER: [" + navigator.userAgent + "]";


//	Maintain window size (according to browser used) while wait message is displayed and the chat request is processed
resizeWindow(438, 438, 438, 415, 408, 381, 0);

return true;
}

</script>

  	</form>
  </div>

  <script src="<c:url value="/js/jquery-2.2.4.min.js" />"></script>
  <script src="<c:url value="/js/bootstrap.min.js" />"></script>
</body>
</html>
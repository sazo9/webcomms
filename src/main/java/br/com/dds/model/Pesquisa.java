package br.com.dds.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Pesquisa {
	
	@Id
	@GeneratedValue
	private Long id;
	private String agente;
	private String pergunta1;
	private String pergunta2;
	private String pergunta3;
	private String pergunta4;
	private String ani;
	private String contaContrato;
	private String nomeApp;
	private Date dataHora;
	
//	public Pesquisa(String agente, String pergunta1, String pergunta2, String pergunta3, String pergunta4, String ani, String contaContrato, String nomeApp, Date dataHora) {
//		this.agente = agente;
//		this.pergunta1 = pergunta1;
//		this.pergunta2 = pergunta2;
//		this.pergunta3 = pergunta3;
//		this.pergunta4 = pergunta4;
//		this.ani = ani;
//		this.contaContrato = contaContrato;
//		this.nomeApp = nomeApp;
//		this.dataHora = dataHora;
//	}
	
//	public Pesquisa(Pesquisa p){
//		this.agente = p.getAgente();
//		this.nota1 = p.getNota1();
//		this.nota2 = p.getNota2();
//		this.nota3 = p.getNota3();
//		this.nota4 = p.getNota4();
//	}
	
	public Pesquisa(){ }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getPergunta1() {
		return pergunta1;
	}

	public void setPergunta1(String pergunta1) {
		this.pergunta1 = pergunta1;
	}

	public String getPergunta2() {
		return pergunta2;
	}

	public void setPergunta2(String pergunta2) {
		this.pergunta2 = pergunta2;
	}

	public String getPergunta3() {
		return pergunta3;
	}

	public void setPergunta3(String pergunta3) {
		this.pergunta3 = pergunta3;
	}

	public String getPergunta4() {
		return pergunta4;
	}

	public void setPergunta4(String pergunta4) {
		this.pergunta4 = pergunta4;
	}

	public String getAni() {
		return ani;
	}

	public void setAni(String ani) {
		this.ani = ani;
	}

	public String getContaContrato() {
		return contaContrato;
	}

	public void setContaContrato(String contaContrato) {
		this.contaContrato = contaContrato;
	}

	public String getNomeApp() {
		return nomeApp;
	}

	public void setNomeApp(String nomeApp) {
		this.nomeApp = nomeApp;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	
	
	
	
	

}

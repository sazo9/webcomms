<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@page import="com.nortel.applications.ccmm.ci.datatypes.*"%>
<%@page import="com.nortel.applications.ccmm.ci.webservices.*"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page buffer="8kb" %>
<%@ page info="Chat" %>

<%

   //Avaya Aura Contact Center Multimedia 
   //Copyright � 2010 Avaya Inc. All Rights Reserved


	String 		sessionKey;
	String 		contactID;

	String ccmmHostname  = application.getInitParameter("CCMM_HOSTNAME");

	request.setCharacterEncoding("utf-8");
	sessionKey=request.getParameter("sessionKey");
	contactID=request.getParameter("contactID");


	// create the session 

	try
   	{

		java.net.URL ciWebCommsWsUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CIWebCommsWs.asmx"); 

		CIWebCommsWs webCommsWsLocator = new CIWebCommsWsLocator();
		CIWebCommsWsSoap webCommsWs = webCommsWsLocator.getCIWebCommsWsSoap( ciWebCommsWsUrl );

		long result = webCommsWs.createWebCommsSession(new Long(contactID).longValue(), sessionKey);    

	}
   	catch(Exception e)
   	{
	     // response.sendRedirect("closed.jsp"); 	
	}



%>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chat</title>
    <link rel="stylesheet" href="<c:url value="/css/main.css" />">
    <script type="text/javascript">
    
	   function post(url, data) {
	       return $.ajax({
	           type: 'POST',
	           url: url,
	           headers: {
	               'Accept': 'application/json',
	               'Content-Type': 'application/json'
	           },
	           data: JSON.stringify(data)
	       })
	   }
	   
	   function sendMessage() {
		    var $messageInput = $('#messageInput');
		    var message = {message: $messageInput.val(), from: userName};
		    $messageInput.val('');
		    post('/chat', message);
		}	   
    
    </script>
</head>
<body>

<div id="container">
    <div id="chat">
        <ul id="messages">

        </ul>
        <form onsubmit="sendMessage(); return false;">

            <div class="chat-message clearfix">
                <input name="message-to-send" autocomplete="off" id="messageInput" placeholder="Digite sua mensagem" />

                <button type="submit">Send</button>
            </div>
        </form>
    </div>
</div>

</body>
</html>



﻿<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Chat</title>
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/normalize.css" rel="stylesheet" type="text/css">

</head>

<body>
<header id="banner">
<div>
	<img src="../assets/img/fundo.png">
</div>
</header>

<main id="main">
<table id="webchat_user_input" class="input_fields" width="448"  border="0" cellSpacing="3" cellPadding="0">
  <tbody>
  <tr>

<td colspan="3"> <p class="lead" style="margin-left:20px;">Esta com dúvida? Precisa de ajuda? Nós te ajudamos! Por favor preencha os campos abaixo</p></td>
</tr>

    <tr>
      <td width="100">&nbsp;</td>
      <td><input name="username" type="field" class="form-control input-sm" id="username" placeholder="Nome" size="30" maxlength="30" ></td>
      <td width="100">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input class="form-control input-sm" type="field" maxlength="30" id="cpf" name="cpf" size="30" placeholder="CPF" ></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input class="form-control input-sm" type="field" maxlength="30" id="email" name="email" size="30" placeholder="E-mail" onKeyPress="return checkSpace(event)"></td>
      <td>&nbsp;</td>
    </tr>
    

    
<tr>

<td colspan="3" align="center" valign="middle" style="padding-top:30px;"><button type="button" class="btn btn-primary btn-lg"> Iniciar Chat</button></td>
</tr>	
  </tbody>
</table>

</main>
</body>
</html>


<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@page import="com.nortel.applications.ccmm.ci.datatypes.*"%>
<%@page import="com.nortel.applications.ccmm.ci.webservices.*"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page buffer="8kb" %>
<%@ page info="Chat" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<HTML>
 <HEAD>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Sample Web Chat</title>
    <link rel="stylesheet" href="<c:url value="/css/style.css" />">
    
    <script type="text/javascript"> 
    var elementID;

    // Resize the window to account for advert area & webchat history
    function onLoadDo()
    {
            //	On window load resize
    	resizeWindow(448, 448, 448, 450, 433, 397, 0);
    }

    //	Resize pop-up according to browser used, and by possible extending length
    function resizeWindow(ffPixelWidth, crPixelWidth, iePixelWidth, ffPixelLength, crPixelLength, iePixelLength, extendLength)
    {
            if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))
                    window.resizeTo(ffPixelWidth, ffPixelLength + extendLength);	//	Firefox
            else if(navigator.userAgent.indexOf("Chrome") != -1)
                    window.resizeTo(crPixelWidth, crPixelLength + extendLength);	//	Chrome
            else if(navigator.appName == "Microsoft Internet Explorer")
            {
                    extendLength -= extendLength/14;
                    window.resizeTo(iePixelWidth, iePixelLength + extendLength);	//	IE
            }
            else 
                    window.resizeTo(iePixelWidth, iePixelLength + extendLength);	//	Resize others as of IE
    }

    // Called by chat frameset to close the chat window when the agent is finished
    function closeChatWindow()
    {
            //	Exit the chat window
            window.close();
    }

    </script>        
        
 </HEAD>
 

  <FORM name=hiddenForm>

<%

   //Avaya Aura Contact Center Multimedia 
   //Copyright � 2014 Avaya Inc. All Rights Reserved


	String 		sessionKey;
	String 		contactID;
      

	request.setCharacterEncoding("utf-8");
	sessionKey=session.getAttribute("sessionKey").toString();
	contactID=session.getAttribute("contactID").toString();
       

%>

<FRAMESET class="light_background" onload="Javascript:onLoadDo()" ROWS="20%, 62%, 20% , 0%, 0%" border="0">

<FRAME SRC="/WebComms/chat/banner" name="banner" scrolling=none>

<FRAMESET COLS="100%" border="0" >
    <FRAME src="/WebComms/chat/chatdisplay64" name="chatdisplay" scrolling=none>
	<FRAME SRC="/WebComms/chat/onhold64" name="maincontrol" scrolling=none>
</FRAMESET>

<FRAMESET COLS="100%" border="0">
	<FRAME SRC="/WebComms/chat/chatwrite64" name="chatwrite" scrolling=none>
	<FRAME SRC="/WebComms/chat/status64" name="statuscontrol" scrolling=none>
	
</FRAMESET>

<FRAME SRC="/WebComms/chat/hidden64" name="wcControl"> <!-- Refreshes every 3 seconds to update chat history and agent messages -->
<FRAME SRC="/WebComms/chat/heartbeat" name="wcHeartbeat"> <!-- Session heartbeat. Runs every 30 seconds -->

</FRAMESET><noframes></noframes>

 </FORM>
       
 <BODY></BODY>
</HTML>

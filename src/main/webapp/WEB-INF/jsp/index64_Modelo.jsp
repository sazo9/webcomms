
<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@page import="com.nortel.applications.ccmm.ci.datatypes.*"%>
<%@page import="com.nortel.applications.ccmm.ci.webservices.*"%>
<%@page import="org.slf4j.Logger"%>
<%@page import="org.slf4j.LoggerFactory"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page buffer="8kb" %>
<%@ page info="Chat" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<HTML>
 <HEAD>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Sample Web Chat</title>
    <link rel="stylesheet" href="<c:url value="/css/style.css" />">
    
    <script type="text/javascript"> 
    var elementID;

    // Resize the window to account for advert area & webchat history
    function onLoadDo()
    {
            //	On window load resize
            resizeWindow(1100, 1000, 1000, 692, 633, 606, 0);
    }

    //	Resize pop-up according to browser used, and by possible extending length
    function resizeWindow(ffPixelWidth, crPixelWidth, iePixelWidth, ffPixelLength, crPixelLength, iePixelLength, extendLength)
    {
            if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))
                    window.resizeTo(ffPixelWidth, ffPixelLength + extendLength);	//	Firefox
            else if(navigator.userAgent.indexOf("Chrome") != -1)
                    window.resizeTo(crPixelWidth, crPixelLength + extendLength);	//	Chrome
            else if(navigator.appName == "Microsoft Internet Explorer")
            {
                    extendLength -= extendLength/14;
                    window.resizeTo(iePixelWidth, iePixelLength + extendLength);	//	IE
            }
            else 
                    window.resizeTo(iePixelWidth, iePixelLength + extendLength);	//	Resize others as of IE
    }

    // Called by chat frameset to close the chat window when the agent is finished
    function closeChatWindow()
    {
            //	Exit the chat window
            window.close();
    }

    </script>        
        
 </HEAD>
 

  <FORM name=hiddenForm>

<%

   //Avaya Aura Contact Center Multimedia 
   //Copyright � 2014 Avaya Inc. All Rights Reserved


	String 		sessionKey;
	String 		contactID;
      
        Logger log = LoggerFactory.getLogger(this.getClass());
	String ccmmHostname  = application.getInitParameter("CCMM_HOSTNAME");

	request.setCharacterEncoding("utf-8");
	sessionKey=request.getParameter("sessionKey");
	contactID=request.getParameter("contactID");
       
        log.info(">>> INDEX64 sessionKey: " + sessionKey);
        log.info(">>> INDEX64 contactID: " + contactID);

	// create the session based on passed in session and contact ID's
	try
   	{
            
                
		java.net.URL ciWebCommsWsUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CIWebCommsWs.asmx"); 
                log.info(">>> ciWebCommsWsUrl: " + ciWebCommsWsUrl);
                
		CIWebCommsWs webCommsWsLocator = new CIWebCommsWsLocator();
		CIWebCommsWsSoap webCommsWs = webCommsWsLocator.getCIWebCommsWsSoap( ciWebCommsWsUrl );
                log.info(">>> webCommsWs " + webCommsWs);

		long result = webCommsWs.createWebCommsSession(new Long(contactID).longValue(), sessionKey);    
                 log.info(">>> result " + result);
	}
   	catch(Exception e)
   	{
            log.info(">>> erro: " + e.getMessage());
	     // response.sendRedirect("closed.jsp"); 	
	}

%>

<FRAMESET ROWS="54%,24%,19%,0%,1%" cols="*" border="1" class="light_background" onload="Javascript:onLoadDo()">
  <FRAME src="/WebComms/chat/chatdisplay64" name="chatdisplay">
  <FRAME SRC="/WebComms/chat/chatwrite64" name="chatwrite">

<FRAME SRC="/WebComms/chat/status64" name="statuscontrol" scrolling=none>

<FRAME SRC="/WebComms/chat/hidden64" name="wcControl"> <!-- Refreshes every 3 seconds to update chat history and agent messages -->
<FRAME SRC="/WebComms/chat/heartbeat" name="wcHeartbeat"> <!-- Session heartbeat. Runs every 30 seconds -->

</FRAMESET><noframes></noframes>

 </FORM>
       
 <BODY></BODY>
</HTML>

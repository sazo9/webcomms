<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title></title>

  <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
  <link rel="stylesheet" href="<c:url value="/css/assunto.css" />">
</head>

  <script src="<c:url value="/js/jquery-3.1.0.min.js" />"></script>
  <script src="<c:url value="/js/bootstrap.min.js" />"></script>
  
<body onload="onLoginLoadDo()">

  <div class="login-form col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
  	<header>
  		<h1><img class="img-responsive" src="<c:url value="/img/fundo.png"/>"></h1>
  		<h2 class="">Olá <span>${username}</span>, estamos aqui para lhe ajudar! Por favor nos diga qual assunto da nossa conversa.</h2>
  	</header>
  	<form  name="requestWebChatform" onsubmit="return processChatRequest()" method="post" action="/WebComms/chat/start">

  		<div class="form-group">
  			<div class="input-group col-xs-12">
          <select class="form-control input-sm" id="skillset" name="skillset">
          	<option value="WC_Default_Skillset"></option>
          	<option value="10007614">2&#170; via de boleto</option>
          	<option value="10007615">Alterar dados cadastrais</option>
          	<option value="10007617">Andamento do Pedido</option>
          	<option value="10007618">Brinde</option>
          	<option value="10007619">Cancelar pedido</option>
          	<option value="10007620">C&#243;digo de rastreio do pedido</option>
          	<option value="10007621">Cupom</option>
          	<option value="10007622">D&#250;vidas de como usar produto</option>
          	<option value="10007623">Elogios</option>
          	<option value="10007624">Estorno</option>
          	<option value="10007625">Produto com defeito ou quebrado</option>
          </select>
  			</div>
  		</div>

  		<footer>
  			<button type="submit" class="btn btn-primary col-xs-5 col-xs-offset-5" style="width: 96px;">Iniciar Chat</button>
  		</footer>

<jsp:include page="include/main_pre_body.jsp" />

<script type="text/javascript"> 
var elementID;


function onLoginLoadDo()
{
//	On window load resize
resizeWindow(448, 448, 448, 415, 433, 397, 0);

document.getElementById('skillset').focus();
}

function replaceWithHTMLEntities(str)
{
//	Replace certain 'vulnerable' characters with their html entities
str = str.replace("<", "&#60;");
str = str.replace(">", "&#62;");
str = str.replace("$", "&#36;");
str = str.replace("[", "&#91;");
str = str.replace("[", "&#93;");
return str; 
}

function processChatRequest()
{

elementID = 0;

//	Retrieve platform and browser information to pass to agent
var WebCommsQuery = "PLATFORM: [" + navigator.platform + "]; BROWSER: [" + navigator.appName + "]; HEADER: [" + navigator.userAgent + "]";

//	Maintain window size (according to browser used) while wait message is displayed and the chat request is processed
resizeWindow(448, 448, 448, 415, 408, 381, 0);


return true;
}

</script>
  	</form>
  </div>


</body>
</html>
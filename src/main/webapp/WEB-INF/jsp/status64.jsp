<%
//Avaya Aura Contact Center Multimedia 
//Copyright 2014 Avaya Inc. All Rights Reserved.
    
String 		sessionKey;
String 		contactID;

sessionKey=session.getAttribute("sessionKey").toString();
contactID=session.getAttribute("contactID").toString();
%>

<HTML>
    <head>
  <style type="text/css">

    .status
    {
	background-color:transparent;
	border: 0px;
	color: CC0000;
	font-weight: bold;
    }

    .queue
    {
	background-color:transparent;
	border: 0px;
	color: CC0000;
    }

    .isWriting
    {
	background-color:transparent;
	border: 0px;
	color: CC0000;
	font-style: italic;
    }

    .table
    {
	position: absolute; top: 1px;
    }

  </style>
  
  <script language="JavaScript">

        // This function is called from the customer chat window.
        // It sets a "customerTyping" hidden field and POSTS to the
        // hiden64.jsp file. Based on this value, the AACC server is
        // notified of the customer typing status.
        function InformCustomerIsTyping(bIsTyping)
        {
            if (bIsTyping)
            {
                document.statusform.customerTyping.value = "1";
            }
            else       
            {
                document.statusform.customerTyping.value = "0";
            }
            
            document.statusform.submit();
        }
        
        // This function is used to reload the hidden64.jsp page.
        // Typically called afater a Customer submits a chat text from
        // the chat text area.
        function RefreshChatHistory()
        {
            // Ensure that customerTyping is not set.
            document.statusform.customerTyping.value = "0";
            // submit and POST to hidden64.jsp.
            document.statusform.submit();
        }
        
  </script>
    </head>

<body>
<!-- On Submit POST hidden data to hidden64.jsp -->
<div class="Container">
<form name="statusform" method="POST" action="/WebComms/chat/hidden64" target="wcControl">
	<div class="form-group">
    <input type=text name="status" class="status" value="Status: Not Connected" size=10>
    </div>
    <div class="">
    
    </div>
    <div class="form-group">
    
    </div>
    <input type=hidden name="expectedWaitTime" size=32 disable>
    <input type=hidden name="positionInQueue" size=20 disable>
    <input type=hidden name="inSession" disable>
    <input type=hidden name="customerTyping" value="0" disable>
</form>
</div>
</BODY>
</HTML>

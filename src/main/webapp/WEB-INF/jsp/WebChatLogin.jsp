<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title></title>

<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
<link rel="stylesheet" href="<c:url value="/css/login.css" />">
</head>

  <script src="<c:url value="/js/jquery-3.1.0.min.js" />"></script>
   <script src="<c:url value="/js/jquery.maskedinput.min.js" />"></script>
  <script src="<c:url value="/js/bootstrap.min.js" />"></script>
  
  
<body onload="onLoginLoadDo()">

  <div class="login-form col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
  	<header>
  		<h1><img class="img-responsive" src="<c:url value="/img/fundo.png"/>"></h1>
  		<h2 class="">Esta com dúvida? Precisa de ajuda? Nós te ajudamos! Por favor preencha os campos abaixo</h2>
  	</header>
  	<form name="requestWebChatform" onsubmit="return processChatRequest()" method="post" action="/WebComms/chat/assunto">
  	  		<div class="form-group">
  			<div class="input-group">
  				<div class="input-group-addon">
  					<span class="glyphicon glyphicon-user"></span>
  				</div>
  				<input type="text" id="username" name="username" class="form-control" placeholder="Nome">
  			</div>
  		</div>

  		<div class="form-group">
  			<div class="input-group">
  				<div class="input-group-addon">
            <span class="glyphicon glyphicon-qrcode"></span>
  				</div>
  				<input type="text" id="cpf" name="cpf" class="form-control" placeholder="CPF">
  			</div>
  		</div>

      <div class="form-group">
  			<div class="input-group">
  				<div class="input-group-addon">
  					<span class="glyphicon glyphicon-envelope"></span>
  				</div>
  				<input type="text" id="email" name="email" class="form-control" placeholder="E-mail">
  			</div>
  		</div>

  		<footer>
  				<button type="submit" class="btn btn-primary col-xs-6 col-xs-offset-5">Enviar</button>
  		</footer>

<jsp:include page="include/main_pre_body.jsp" />

<script type="text/javascript"> 
var elementID;




function onLoginLoadDo()
{
//	On window load resize
resizeWindow(438, 438, 438, 415, 433, 397, 0);

//	Set focus to name field
document.getElementById('username').focus();
}

function replaceWithHTMLEntities(str)
{
//	Replace certain 'vulnerable' characters with their html entities
str = str.replace("<", "&#60;");
str = str.replace(">", "&#62;");
str = str.replace("$", "&#36;");
str = str.replace("[", "&#91;");
str = str.replace("[", "&#93;");
return str; 
}

function processChatRequest()
{

var error = false;
elementID = 0;

//	Get input
var username = replaceWithHTMLEntities(document.getElementById('username').value);
var cpf = replaceWithHTMLEntities(document.getElementById('cpf').value);
var email = replaceWithHTMLEntities(document.getElementById('email').value);
//var phone = replaceWithHTMLEntities(document.getElementById('phone').value);

//	Retrieve platform and browser information to pass to agent
var WebCommsQuery = "PLATFORM: [" + navigator.platform + "]; BROWSER: [" + navigator.appName + "]; HEADER: [" + navigator.userAgent + "]";

//	Assign username to first name for creating user in database
FirstName = username;

document.getElementById('email_error').innerHTML = "";
//document.getElementById('phone_error').innerHTML = "";




//	Validate given email address
if(email != "")
{
	//	If string contains whitespace, trim
	email = whitespaceTrim(email);
	//	 Validate email address
	if(!isValidEmailAddress(email))
	{
		document.getElementById('email_error').innerHTML = "<font color='red'>Email Inválido</font>";
		$('#divEmail').addClass( "has-error" );
		error = true;
		elementID = document.getElementById('email');
	}
	else
	{
		$('#divEmail').removeClass( "has-error" ).addClass("has-success");
	}
	
}else{
	if(!isValidEmailAddress(email))
	{
		document.getElementById('email_error').innerHTML = "<font color='red'>Email Inválido</font>";
		$('#divEmail').addClass( "has-error" );
		error = true;
		elementID = document.getElementById('email');
	}
	else
		{
			$('#divEmail').removeClass( "has-error" ).addClass("has-success");
		}
}

//	Validate given phone number
//if(phone != "")
//{
//	If string contains whitespace, trim
//	phone = whitespaceTrim(phone);
//	 Validate phone number
//	if(!isValidPhoneNumber(phone))
//	{
//		document.getElementById('phone_error').innerHTML = "<font color='red'>Invalid Phone Number</font>";
//		error = true;
//		if (elementID == 0)
//			elementID = document.getElementById('phone');
//	}
//}

// Check for error
if(error)
{
//	Set element focus
elementID.focus();
return false;
}

//	Hide user input panel
document.getElementById('webchat_user_input').style.display = "none";

//	Show loading panel while we do all the web service calls and it can take a few seconds to complete
document.getElementById('webchat_please_wait').style.display = "block";

//	Maintain window size (according to browser used) while wait message is displayed and the chat request is processed
resizeWindow(438, 438, 438, 415, 408, 381, 0);



return true;
}

jQuery(function($) {
    $.mask.definitions['~']='[+-]';
    $('#cpf').mask('999.999.999-99');
 });
</script>

  	</form>
  </div>



</body>
</html>

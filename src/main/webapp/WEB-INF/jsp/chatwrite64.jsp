<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="com.nortel.applications.ccmm.ci.datatypes.*"%>
<%@ page import="com.nortel.applications.ccmm.ci.webservices.*"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page buffer="8kb" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%

   //Avaya Aura Contact Center Multimedia 
   //Copyright � 2014 Avaya Inc. All Rights Reserved

   String 		sessionKey;
   String 		contactID;
   String		message;
   String 		chatbox="";
   String		hiddenMessage="";
   String 		type="";
   
   String ccmmHostname  = session.getAttribute("CCMM_HOSTNAME").toString();
   
	  request.setCharacterEncoding("utf-8");
	sessionKey=session.getAttribute("sessionKey").toString();
	contactID=session.getAttribute("contactID").toString();
	  message=request.getParameter("message");
	  chatbox=request.getParameter("chatbox");
	  hiddenMessage=request.getParameter("hiddenMessage");
	  type=request.getParameter("type");
   
   // Customer Question
   // String onHoldQuestion="";
   // onHoldQuestion = request.getParameter("onHoldQuestion");
   
   if (chatbox == null)
   {
	chatbox ="";
   }

   if (message == null)
   {
   	message = "null";
   }

   if (message.indexOf("null") == -1)
   {
	try
   	{
	
		java.net.URL ciWebCommsWsUrl = new java.net.URL("http://" + ccmmHostname + "/ccmmwebservices/CIWebCommsWs.asmx"); 

		CIWebCommsWs webCommsWsLocator = new CIWebCommsWsLocator();
		CIWebCommsWsSoap webCommsWs = webCommsWsLocator.getCIWebCommsWsSoap( ciWebCommsWsUrl );

		// We can either prefix the written message with a generic customer prefix
		// as configured in the CCMM Administrator or alternatively we can choose to
		// prefix the message by the customer name
		
                // Option1: Uncomment the section below to use the default [Customer] tag, as per CCMM Admin:
		/// String custLabel = webCommsWs.getCustomerTextChatLabel(sessionKey);

                // Option2: Get the name based on the contact ID:
                // Note: This can be empty/null in the case of a an anonymous chat implementation.
                
                String custLabel = "";
		CICustomerChatNameReadType customerName = webCommsWs.getCustomerTextChatName(new Long(contactID).longValue(), sessionKey);
                
                if(customerName.getFirstName() != null) // This may not be set if the session is an anonymous chat session:
                {
                    // Set the Customer label to be the Customer first name:
                    custLabel = customerName.getFirstName() + ": ";
                    session.setAttribute("nome_cliente", customerName.getFirstName());
                }
                else
                {
                    custLabel = webCommsWs.getCustomerTextChatLabel(sessionKey);
                }
                                
		message = custLabel + " " + message;

   		// We must write the correct message type with the text. This allows the agent to correctly process
   		// the message e.g. display pushed page. The hiddenMessage is the URL of a pushed page.

		if(type.equals("Chat_Message_from_Customer"))
		{
			long status = webCommsWs.writeChatMessage(new Long(contactID).longValue(), message, "", CIChatMessageType.Chat_Message_from_Customer, sessionKey);    
		}
		else if (type.equals("Page_Pushed_by_Customer"))
		{
			long status = webCommsWs.writeChatMessage(new Long(contactID).longValue(), message, hiddenMessage, CIChatMessageType.Page_Pushed_by_Customer, sessionKey);    
		}
		else if (type.equals("Form_Shared_by_Customer"))
		{
			long status = webCommsWs.writeChatMessage(new Long(contactID).longValue(), message, hiddenMessage, CIChatMessageType.Form_Shared_by_Customer, sessionKey);    
		}
		else if (type.equals("CallMe_Request_from_Customer"))
		{
			long status = webCommsWs.writeChatMessage(new Long(contactID).longValue(), message, hiddenMessage, CIChatMessageType.CallMe_Request_from_Customer, sessionKey);    
		}
		else if (type.equals("Page_Previewed_by_Customer"))
		{
			long status = webCommsWs.writeChatMessage(new Long(contactID).longValue(), message, hiddenMessage, CIChatMessageType.Page_Previewed_by_Customer, sessionKey);    
		}
		else if (type.equals("Session_Disconnected_by_Customer"))
		{
			long status = webCommsWs.writeChatMessage(new Long(contactID).longValue(), message, hiddenMessage, CIChatMessageType.Session_Disconnected_by_Customer, sessionKey);    
		}

	}
   	catch(Exception e)
   	{
   	
   		// We caught an exception while trying to write a chat message. The session has been disconnected. 
   		// Inform the customer that the agent has left the conversation and update the status.
   	
		%><script language="Javascript">
			parent.chatdisplay.document.chatform.chatterbox.value = parent.chatdisplay.document.chatform.chatterbox.value + "\n" + "Agent has left the conversation.";
			parent.chatdisplay.document.chatform.chatterbox.scrollTop = parent.chatdisplay.document.chatform.chatterbox.scrollHeight;
			parent.statuscontrol.document.statusform.status.value = "Session Disconnected";
			parent.statuscontrol.document.statusform.inSession.value = "0";

		</script><%
	}
   }

%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
  <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
  <link rel="stylesheet" href="<c:url value="/css/chatwrite.css" />">
  


  
<script type="text/javascript">
    function crunchifyAjax() {
        $.ajax({
            url : '/WebComms/chat/pesquisa',
            success : function(data) {
                $('#result').html(data);
            }
        });
    }
</script>

<script LANGUAGE="JavaScript">

  NS4 = (document.layers) ? true : false;

  function checkEnter(event)
  { 	
	// If the enter key was pressed, send the message or assume a page preview.
	var code = 0;
       
	if (NS4)
		code = event.which;
	else
		code = event.keyCode;
            
	if (code==13)
	{
		sendChat();
		parent.chatwrite.document.chatform.submit();
                //parent.statuscontrol.RefreshChatHistory();
	}
        else
        {
           // NOTE: This exmaple implements the "Customer Is Typing"  feature
           // each time a key is pressed. To reduce the load on the server, this can
           // be reduced by only allowing this once per 'pre-submit' time interval.
            
           // Check if the customer is typing in the chat text-area: 
           // If text remmains in the text-area inform the agent that the
           // customer is typing a message:
           if (document.chatform.customerchat.value.length > 0)
           {
               parent.statuscontrol.InformCustomerIsTyping(true);
           }
           else // Customer has removed any text and is no longer typing: 
           {
               parent.statuscontrol.InformCustomerIsTyping(false);
           }
        }
  }
  
  
  function setfocus()
  {
  	document.chatform.customerchat.focus();

  }


  function sendChat()
  {
    // Send the chat message only if there is an active session and there is a message to send.

    if(parent.statuscontrol.document.statusform.inSession.value=="0")
    {
	alert("Não é possivel enviar a mensagem pois não existe um agente ativo nesta conversa.");
    }
    else
    {
    	
	if(parent.chatwrite.document.chatform.customerchat.value=="")
	{
	
	  // do nothing as the user has not entered a chat message
	  parent.chatwrite.document.chatform.message.value = "null";

	}
	else
	{
	  parent.chatwrite.document.chatform.customerchat.value = " " + parent.chatwrite.document.chatform.customerchat.value;
	  parent.chatwrite.document.chatform.message.value = parent.chatwrite.document.chatform.customerchat.value;
	  parent.chatwrite.document.chatform.type.value = "Chat_Message_from_Customer";
	  parent.chatwrite.document.chatform.hiddenMessage.value = "";
	  parent.chatwrite.document.chatform.customerchat.value = "";
          
          
	  message = null;
	}
    }
  }

  function printChat()
  {
     window.parent.chatdisplay.focus();
     window.print();
  }

  function exitChat()
  {
  	// Terminate the session if the user confirms.
  
  	var isSessionActive = parent.statuscontrol.document.statusform.inSession.value;
  	
	var answer = confirm ("Tem certeza que deseja sair do Chat?") 
	if (answer) 
	{
		// Close the session:
		// Session cleanup code is contained in exitFormImpl64.jsp
		
// 		window.location.href = "http://localhost:9090/WebComms/pesquisa";
				
		parent.displayChat.document.body.innerHTML += '<form id="dynForm" action="/WebComms/pesquisa" name="q" value="a"></form>';
		parent.displayChat.document.getElementById("dynForm").submit();
	}
	
  }
  
  function checkSpace(event)
  { 	
	return event.which !== 32;
  }
  


  // This function is called from the customer chat window.
  // It sets a "customerTyping" hidden field and POSTS to the
  // hiden64.jsp file. Based on this value, the AACC server is
  // notified of the customer typing status.
  function InformCustomerIsTyping(bIsTyping)
  {
      if (bIsTyping)
      {
          document.statusform2.customerTyping.value = "1";
      }
      else       
      {
          document.statusform2.customerTyping.value = "0";
      }
      
      document.statusform2.submit();
  }
  
  // This function is used to reload the hidden64.jsp page.
  // Typically called afater a Customer submits a chat text from
  // the chat text area.
  function RefreshChatHistory()
  {
      // Ensure that customerTyping is not set.
      document.statusform2.customerTyping.value = "0";
      // submit and POST to hidden64.jsp.
      document.statusform2.submit();
  }
  

</script>

</head>

  <script src="<c:url value="/js/jquery-3.1.0.js" />"></script>
  
<body onload=setfocus()>

 <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
 <div class="container">
       <form name="statusform2" method="POST" action="/WebComms/chat/hidden64" target="wcControl">
       <table>
       <tbody>
       <tr>
       <th>

         <h6 class="text-center" id="isWriting">
         </h6>

       </th>
       </tr>
       </tbody>
       </table>
       </form>

      <form name="chatform" method="POST">
       <table>
           <tbody>
             <tr>
               <th rowspan="2" class="thTextarea">
                   <textarea class="form-control textarea" id="messageInput" name="customerchat" rows=3 onkeypress="checkEnter();" placeholder="Digite sua mensagem."></textarea>
                   <input type="hidden" name="message" value="<%=message%>">
                   <input type="hidden" name="chatbox" value="<%=chatbox%>">
                   <input type="hidden" name="type" value="<%=type%>">
                   <input type="hidden" name="hiddenMessage" value="<%=hiddenMessage%>">
               </th>
               <th colspan="3" class="enviar"><button class="btn btn-primary botaoEnviar" id='enterButton' type="submit" onclick="sendChat();">Enviar</button></th>
             </tr>
             <tr>
               <th class="sair">
               		<button class="btn btn-primary botaoSair" id='exitButton' onclick="crunchifyAjax();">Sair</button>
               	</th>
             </tr>
           </tbody>
         </table>
     </form>

     <div class="form-group">
       <form name="statusform" method="POST" action="/WebComms/chat/hidden64" target="wcControl">
       <table>
       <tbody>
       <tr>
       <th>
       <div class="text-center">
         <h6>
 			<input type=hidden name="expectedWaitTime" size=32 disable>
 			<input type=hidden name="positionInQueue" size=20 disable>
 			<input type=hidden name="inSession" disable>
 			<input type=hidden name="customerTyping" value="0" disable>
 			<div id="result"></div>
         </h6>
       </div>
       </th>
       </tr>
       </tbody>
       </table>
       </form>
       </div>


 </div>
     </div>
    
</body>
</HTML>
